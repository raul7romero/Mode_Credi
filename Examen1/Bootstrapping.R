bootstrapping <- function(data_test,  n, breaks, reference_var, good_id, 
                          bad_id, marketing, client_mkt){
  require(ggplot2)
  require(matrixStats)
  source('histogram_separate.R')
  source("utilities_obtain.R")
  source('utilities_obtain_V2.R')
  
  # Esta funci�n te har� un bootstrapping donde n es el n�mero de veces que quieres hacer el muestreo.
  # Breaks lo obtienes de la funci�n previa utilities_obtain_V2 en caso de no tenerlo en el c�digo.
  # Requiere de la funci�n cum_utilities
  
  score_cuts <- breaks[1:(length(breaks)-1)]
  bootstrap_df <- matrix(, nrow = n, ncol = length(score_cuts))
  
  
  for(bt in 1:n){
  
    bootstrap_rows <- sample(row.names(data_test), 
                             nrow(data_test), 
                             replace = T)
    
    bootstrap_sample   <- data_test[bootstrap_rows, ]
    
    data <- separ_hist(bootstrap_sample, reference_var, good_id, bad_id)
    
    bootstrap_util     <- utilities_obtain_v2(bootstrap_sample, reference_var, good_id,
                                              bad_id, marketing, client_mkt, 
                                              data$`Accepted/default per bin`[1, ], 
                                              data$`Accepted/default per bin`[2, ], 
                                              breaks)
    
    bootstrap_df[bt, ]       <- bootstrap_util$Cumulative_utilities
    
  }
  
  
  colnames(bootstrap_df) <- as.character(score_cuts)
  means <- colMeans(bootstrap_df)
  sds   <- colSds(bootstrap_df)
  names(sds) <- as.character(score_cuts)
  
  minor <- means - sds
  mayor <- means + sds
  
  # El objetivo es construir modelos que con todo y variaci�n est�n por arriba del cero.
  
  plot_data <- as.data.frame(t((rbind(score_cuts, means, minor, mayor))))
  
  ggplot(data = plot_data, aes(x = plot_data$score_cuts)) + 
    geom_line(aes(y = plot_data$means , colour = "means"))+
    geom_line(aes(y= plot_data$minor, colour = "minor"))+
    geom_line(aes(y=plot_data$mayor, colour = "mayor"))+
    xlab("Score cuts") + ylab("Value")+
    scale_colour_manual("", 
                        breaks = c("means", "minor", "mayor"),
                        values = c("red", "dark blue", "red")) 
  
  # datos <- list()
  # datos[[1]] <- minor
  # datos[[2]] <- mayor
  # datos[[3]] <- means
  # names(datos) <- c('Minor', 'Mayor', 'Means')
  # return(datos)
  
  }

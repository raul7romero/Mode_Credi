\select@language {spanish}
\contentsline {section}{\numberline {1}Planteamiento del problema 1}{II}{section.1}
\contentsline {section}{\numberline {2}Lectura de datos}{III}{section.2}
\contentsline {section}{\numberline {3}Inciso A: Bayes model}{III}{section.3}
\contentsline {section}{\numberline {4}Inciso B: Random Forest}{V}{section.4}
\contentsline {section}{\numberline {5}Inciso C: Histogramas}{VII}{section.5}
\contentsline {section}{\numberline {6}Inciso D: Red neuronal}{VIII}{section.6}
\contentsline {section}{\numberline {7}Inciso E: Histograma de red neuronal}{IX}{section.7}
\contentsline {section}{\numberline {8}Inciso F: Promedio de Montos, porcentajes de cr\'edito y cantidades}{X}{section.8}
\contentsline {section}{\numberline {9}Inciso G: Revenue y default per bin}{XIII}{section.9}
\contentsline {section}{\numberline {10}Inciso H: Encontrando score \'optima}{XIII}{section.10}
\contentsline {section}{\numberline {11}Inciso I: Bootstrapping}{XIV}{section.11}
\contentsline {section}{\numberline {12}Anexos}{XV}{section.12}
\contentsline {subsection}{\numberline {12.1}Bayes Model}{XV}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Random Forest}{XV}{subsection.12.2}
\contentsline {subsection}{\numberline {12.3}Red neuronal}{XVI}{subsection.12.3}
\contentsline {subsection}{\numberline {12.4}Funci\'on de separaci\'on de histogramas}{XVII}{subsection.12.4}
\contentsline {subsection}{\numberline {12.5}Funci\'on de obtenci\'on de utilidades}{XVIII}{subsection.12.5}
\contentsline {subsection}{\numberline {12.6}Funci\'on de bootstrapping}{XX}{subsection.12.6}

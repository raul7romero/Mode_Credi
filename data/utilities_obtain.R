utilities_obtain <- function(data_model, reference_var, good_id, 
                             bad_id, interest, marketing, client_mkt,
                             accepted_bin, default_bin, breaks){
  
  # Esta funci�n calcula la utilidad por bin. Se le da un data_model, la tasa de inter�s
  # El costo de marketing y los clientes que trae cada marketing. 
  # Devuelve utilidades por bin y utilidades acumuladas. Depende de previo correr la funci�n
  # histogram_separate. 
  # Los breaks los obtienes de la funci�n histogram_separate en caso de no tenerlos en el c�digo.
  
  score_cuts <- breaks[1:(length(breaks)-1)] #Calificaciones sobre las que har� el corte
  
  
  
  array_amount_good <- c()
  array_amount_bad <- c()
  
  for(index in 1:length(score_cuts)){ #Promediar montos de los clientes que caen en estos valores
    
    #Vamos a usar el data_model para calcular el pr�stamo promedio 
    #aunque lo correcto ser�a usar el data model
    
    average_amount_good <- mean(data_model[data_model$is_good == good_id & 
                                            data_model[, reference_var] >= breaks[index] &
                                            data_model[, reference_var] < breaks[index + 1],
                                          "amount"])
    
    average_amount_bad <- mean(data_model[data_model$is_good == bad_id & 
                                           data_model[, reference_var] >= breaks[index] &
                                           data_model[, reference_var] < breaks[index + 1],
                                         "amount"])
    
    
    if( is.nan(average_amount_good) | is.na(average_amount_good)){
      average_amount_good <- 0
    }
    
    if( is.nan(average_amount_bad) | is.na(average_amount_bad)){
      average_amount_bad <- 0
    }
    
    array_amount_good <- c(array_amount_good, average_amount_good)
    array_amount_bad  <- c(array_amount_bad, average_amount_bad)
    
  }
  
  
  interes <- interest  
  revenue <- client_mkt * accepted_bin * (1 - default_bin) * array_amount_good * interes #arreglo
  
  default_cost <- client_mkt * accepted_bin * default_bin * array_amount_bad #defaulted_craction per bin
  marketing    <- marketing
  utilities    <- revenue - default_cost
  cumulative_utilities <- rev(cumsum(rev(utilities))) - marketing
  
  utilities_gen <- list()
  utilities_gen[[1]] <- utilities
  utilities_gen[[2]] <- cumulative_utilities 
  utilities_gen[[3]] <- score_cuts
  names(utilities_gen) <- c('Utilities', 'Cumulative_utilities', 'Score_cuts')
  
  return(utilities_gen)
  
}

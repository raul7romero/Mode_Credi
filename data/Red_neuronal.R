require('neuralnet')


# *NO CORRER* Preparaci�n previa: *NO CORRER*. ------------------------------------------

df <- read.table("german.data")
varnames <- c('checking_acc','time_credit_acc','credit_history',
              'purpose','amount','savings_acc','p_employment_time',
              'installment_rate','marita_status_sex','other_debtors',
              'p_residence_time','property','age','other_installment',
              'housing','number_of_credits','job','dependants','has_phone',
              'foreign_worker','is_good')
colnames(df) <- varnames
source("transform_data.R")
df <- transform_data(df)


vars_model <- c('amount', 
                'age',
                'credit_history_numeric',
                'purpose_numeric',
                "p_employment_time_numeric",
                "checking_acc_numeric") 

selected_rows <- sort(sample(nrow(df),floor(0.6*nrow(df))))

data_model <- df[selected_rows, c(vars_model, "is_good")] #Datos de entrenamiento
data_test  <- df[-selected_rows, c(vars_model, "is_good")] #Datos de prueba

for(myrow in 1:nrow(df)){
  my_client <- df[myrow, ]
  bayes_prediction <- mybayesmodel(my_client, df)
  
  df[myrow, 'bayesmodel'] <- bayes_prediction
} 


# Corriendo randomforest para todo el df original
df <- read.csv(file = "linnear_regression_data.csv", head = T)
for(test_row in 1:nrow(df)){
  df[test_row, 'randomForest'] <- predict(rf,
                                          df[test_row, ], # Informaci�n del cliente actual
                                          type = 'prob')[2]      # Proporci�n de �rboles que lo tomaron bueno
  
  
} 


# Lectura de datos --------------------------------------------------------

setwd("C:/Users/Ra�l Romero Barrag�n/Documents/Gitlab/MC/Mode_Credi/data")
dfbayes   <- read.csv(file = "bayes_df.csv", head = T)
dfrandomF <- read.csv(file = "rf_df.csv", head = T)


# Metodolog�a: Usando las variables 'mybayesmodel' y 'randomForest' veremos el desempe�o de neuralnet 
# para comparar �sto con una red neuronal entrenada con variables originales


# Funci�n mybayesmodel ----------------------------------------------------

mybayesmodel <- function(info_client, data_model){
  
  #Creaci�n de loop para hacer la multiplicaci�n de productos
  
  
  p_x_GB  <- 1 #Cociente 
  
  for(varname in vars_model){
    hist_var <- hist(data_model[, varname], plot = F) 
    mids_varname <- hist_var$mids
    dif <- as.numeric(mids_varname) - as.numeric(info_client[varname])
    bin <- which.min(abs(dif))
    prob_rel_varname <- prob_obtain(varname, 0)
    choose_prob_rel_varname <- prob_rel_varname[bin]  #Este es el n�mero que ir� multiplicando
    p_x_GB <- p_x_GB * choose_prob_rel_varname
    
    
  }
  
  good <- data_model[data_model$is_good == 1, ]  
  bad <- data_model[data_model$is_good == 2, ]  
  bayes <- log(p_x_GB  * nrow(good) / nrow(bad)) #El logaritmo se comporta mejor al no tener valores 
  #est�pidamente grandes
  
  return(bayes)  
  
}

# Con variables 'mybayesmodel' y 'randomForest' ---------------------------


# Corriendo bayesmodel para todo el dataframe original



df<- cbind(dfbayes$bayesmodel, dfrandomF$randomForest, dfrandomF$is_good)
colnames(df) <- c('bayesmodel', 'randomForest', 'is_good')
df <- as.data.frame(df)
# amount        <- dfbayes$amount
# df$amount <- amount

selected_rows <- sample(nrow(df), floor(0.6*nrow(df)))
var_names     <- colnames(df)[-3]
data_model    <- df[selected_rows, colnames(df)]
data_test     <- df[-selected_rows, colnames(df)]
is_good       <- data_test$is_good 



a <- paste(colnames(data_model)[-3],collapse=' + ')
b <- paste('data_model$is_good ~', a)

nn <- neuralnet(b,
                data_model,
                  hidden = 3, 
                threshold = 0.01) 

# Evaluando modelo 

result <- compute(nn, data_test[, -3])

data_test$nn <- result$net.result
data_test$is_good <- is_good

# Iniciando con histogramas y proceso de evaluaci�n del modelo.

source("histogram_separate.R")
source('utilities_obtain.R')  
source('Bootstrapping.R')

datos_1 <- separ_hist(data_test, 'nn', 1, 0) # Separando histogramas y obteniendo aceptados y default por bin 
utilities <- utilities_obtain(data_test, 'nn', 1, 0, .3, 200000, 1000, 
                               datos_1$`Accepted/default per bin`[1, ],
                               datos_1$`Accepted/default per bin`[2, ],
                               datos_1$breaks)
boots <- bootstrapping(data_test = data_test, n = 30, breaks = datos_1$breaks, 
              reference_var = 'nn', good_id = 1, bad_id = 0, interest = .3,
              marketing = 200000, client_mkt = 1000,
              accepted_bin = datos_1$`Accepted/default per bin`[1, ], 
              default_bin = datos_1$`Accepted/default per bin`[2, ])

# Con variables originales  -----------------------------------------------


df <- read.csv(file = "linnear_regression_data.csv", head = T)

selected_rows <- sample(nrow(df), floor(0.6*nrow(df)))
data_model    <- df[selected_rows, ]
data_model    <- data_model[, -1]       # Le quito el var0
data_test     <- df[-selected_rows, -1] # Le quito el var0

var_names <- colnames(df)
var_names <- var_names[2: (length(var_names)-1)]  


# Iniciando el entrenamiento de datos
a <- paste(colnames(data_model)[-3],collapse=' + ')
b <- paste('data_model$is_good ~', a)

nn <- neuralnet(b,
                data_model,hidden = c(i,j), rep = 10) 

# Iniciando el data_Test

result <- compute(nn, data_test[, -ncol(data_test)], rep=1)

data_test$nn <- result$net.result
data_test$is_good <- is_good


datos_2 <- separ_hist(data_test, 'nn', 1, 0) # Separando histogramas y obteniendo aceptados y default por bin 
